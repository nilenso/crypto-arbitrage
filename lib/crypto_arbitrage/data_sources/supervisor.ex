defmodule CryptoArbitrage.DataSources.Supervisor do
  use Supervisor
  
  @name DataSourcesSupervisor

  def start_link(options \\ []) do
    Supervisor.start_link(__MODULE__, :ok , options ++ [name: @name])
  end
  
  def init(:ok) do
    children = [CryptoArbitrage.DataSources.CoinbaseClient,
                CryptoArbitrage.DataSources.BitfinexClient,
                CryptoArbitrage.DataSources.BitstampClient,
                CryptoArbitrage.DataSources.KrakenClient,
                CryptoArbitrage.DataSources.Gemini.Supervisor]
    options = [strategy: :one_for_one]
    Supervisor.init(children, options)
  end
end
