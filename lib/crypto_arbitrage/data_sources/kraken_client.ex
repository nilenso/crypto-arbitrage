defmodule  CryptoArbitrage.DataSources.KrakenClient do
  alias CryptoArbitrage.DataSources.Common
  use WebSockex

  @url "wss://ws.kraken.com/"
  @cryptos ["BTC", "EOS", "ETH", "LTC", "XRP", "ZEC"]
  @exchange :kraken

  def start_link(_opts) do
    {:ok, pid} = WebSockex.start_link(@url, __MODULE__, %{})
    Common.init_exchange(@exchange, @cryptos)
    subscribe_all(pid)
    {:ok, pid}
  end

  # Message handlers

  def handle_frame({:text, msg}, state) do
    handle_msg(Jason.decode!(msg), state)
  end
  
  ## Subscription 
 
  def handle_msg(%{"event" => "subscriptionStatus", 
                   "status" => "subscribed",
                   "channelID" => channel_id,
                   "pair" => pair}, state) do
    crypto = Common.atomize_crypto(pair)
    new_state = Map.merge(state, %{channel_id => crypto})
    {:ok, new_state}
  end
  
  ## Trades
  def handle_msg([channel_id, trades], state) do
    crypto = Map.get(state, channel_id)
    [price, volume, time, _, _, _] = List.last(trades)
    Common.update_price(@exchange, crypto, price, time, volume)
    {:ok, state}
  end

  ## Heartbeat
  def handle_msg(%{"event" => "heartbeat"}, state) do 
    {:ok, state}
  end  

  ## Other

    def handle_msg(msg, state) do 
    Common.log(@exchange, msg)
    {:ok, state}
  end

  # Subscription helper functions

  defp subscribe_all(pid) do
    Enum.each(@cryptos, &subscribe(&1, pid))
  end

  defp subscribe(crypto, pid) do
    frame = subscription_frame(crypto)
    WebSockex.send_frame(pid, frame)
  end

  defp subscription_frame(crypto) do
    subscription_json =
      %{
        event: "subscribe",
        pair: [crypto <> "/" <> "USD"],
        subscription: %{name: "trade"}
      }
      |> Jason.encode!()

    {:text, subscription_json}
  end

  #
  def cast_time(time) do
   time 
   |>  String.replace(".", "") 
   |> String.to_integer() 
   |> Timex.from_unix(:microsecond)
  end

  def exchange_data do
    %{name: "Kraken",
      key: @exchange,
      cryptos: @cryptos}
  end
  # Common functions
  
  def handle_connect(conn, state) do
    Common.handle_connect(conn, state, @exchange)
  end

  def handle_disconnect(conn, state) do
    Common.handle_disconnect(conn, state, @exchange)
  end

  def child_spec(opts) do
    Common.child_spec(__MODULE__, opts)
  end

end
