defmodule  CryptoArbitrage.DataSources.BitstampClient do
  alias CryptoArbitrage.DataSources.Common
  use WebSockex

  @url "wss://ws.bitstamp.net/"
  @cryptos  ["BTC", "ETH", "LTC", "XRP"]
  @exchange :bitstamp

  def start_link(_opts) do
    {:ok, pid} = WebSockex.start_link(@url, __MODULE__, :no_state)
    Common.init_exchange(@exchange, @cryptos)
    subscribe_all(pid)
    {:ok, pid}
  end

  # Message handlers

  def handle_frame({:text, msg}, state) do
    handle_msg(Jason.decode!(msg), state)
  end
  
  ## Ticker
  def handle_msg(%{"event" => "trade",
                   "channel" => pair,
                   "data" => %{"price" => price,
                               "microtimestamp" => time,
                              "amount_str" => amount}}, state) do
    Common.update_price(@exchange, pair, price, time, amount)
    {:ok, state}
  end

  ## Other

  def handle_msg(msg, state) do 
    Common.log(@exchange, msg)
    {:ok, state}
  end

  # Subscription helper functions

  defp subscribe_all(pid) do
    Enum.each(@cryptos, &subscribe(&1, pid))
  end

  defp subscribe(crypto, pid) do
    frame = subscription_frame(crypto)
    WebSockex.send_frame(pid, frame)
  end

  defp subscription_frame(crypto) do
    subscription_json =
      %{event: "bts:subscribe",
        data: %{channel: "live_trades_" <> String.downcase(crypto) <> "usd"}}
      |> Jason.encode!()

    {:text, subscription_json}
  end
  #
  def cast_time(time) do
    time 
    |>  String.to_integer() 
    |> Timex.from_unix(:microsecond)
  end

  def exchange_data do
    %{name: "Bitstamp",
      key: @exchange,
      cryptos: @cryptos}
  end
  # Common functions

  def handle_connect(conn, state) do
    Common.handle_connect(conn, state, @exchange)
  end

  def handle_disconnect(conn, state) do
    Common.handle_disconnect(conn, state, @exchange)
  end

  def child_spec(opts) do
    Common.child_spec(__MODULE__, opts)
  end

end
