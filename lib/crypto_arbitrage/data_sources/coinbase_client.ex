defmodule  CryptoArbitrage.DataSources.CoinbaseClient do
  alias CryptoArbitrage.DataSources.Common
  use WebSockex

  @url "wss://ws-feed.pro.coinbase.com/"
  @cryptos  ["BTC", "EOS", "ETH", "LTC", "XRP", "ETC"]
  @exchange :coinbase

  def start_link(_opts) do
    {:ok, pid} = WebSockex.start_link(@url, __MODULE__, %{})
    Common.init_exchange(@exchange, @cryptos)
    subscribe_all(@cryptos, pid)
    {:ok, pid}
  end

  # Message handlers

  def handle_frame({:text, msg}, state) do
    handle_msg(Jason.decode!(msg), state)
  end
  
  ## Ticker
  def handle_msg(%{"type" => "ticker",
                   "price" => price,
                   "time" => time,
                   "product_id" => pair,
                   "last_size" => last_size}, state) do
    Common.update_price(@exchange, pair, price, time, last_size)
    {:ok, state}
  end

  ## Heartbeat
  def handle_msg(%{"type" => "heartbeat"}, state) do 
    {:ok, state}
  end

  ## Other

  def handle_msg(msg, state) do 
    Common.log(@exchange, msg)
    {:ok, state}
  end

  # Subscription helper functions

  defp subscribe_all(cryptos, pid) do
    frame = subscription_frame(cryptos)
    WebSockex.send_frame(pid, frame)
  end

  defp subscription_frame(cryptos) do
    subscription_json =
      %{
        type: "subscribe",
        product_ids: product_ids(cryptos),
        channels: ["heartbeat",
                   %{name: "ticker",
                     product_ids: product_ids(cryptos)}]
      }
      |> Jason.encode!()

    {:text, subscription_json}
  end

  defp product_ids(cryptos) do
    Enum.map(cryptos, fn crypto -> crypto <> "-USD" end)
  end
  
  #
  def cast_time(time) do
    {:ok, timestamp, _} = DateTime.from_iso8601(time)
    timestamp
  end

  def exchange_data do
    %{name: "Coinbase",
      key: @exchange,
      cryptos: @cryptos}
  end

  # Common functions

  def handle_connect(conn, state) do
    Common.handle_connect(conn, state, @exchange)
  end

  def handle_disconnect(conn, state) do
    Common.handle_disconnect(conn, state, @exchange)
  end

  def child_spec(opts) do
    Common.child_spec(__MODULE__, opts)
  end

end
