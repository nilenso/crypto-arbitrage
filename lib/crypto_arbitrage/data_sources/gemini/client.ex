defmodule CryptoArbitrage.DataSources.Gemini.Client do
  alias CryptoArbitrage.DataSources.Common
  use WebSockex

  @cryptos ["BTC", "ETH", "LTC", "ZEC"]
  @url "wss://api.gemini.com/v1/marketdata/"
  @exchange :gemini

  def start_link(crypto) do
    WebSockex.start_link(url_string(crypto), __MODULE__, 
      Common.atomize_crypto(crypto))
  end

  # Message handlers

  def handle_frame({:text, msg}, state) do
    handle_msg(Jason.decode!(msg), state)
  end
    
  ## Trades
  def handle_msg(%{"events" => [trade],
                   "timestampms" => time,
                   "type" => "update"}, state) do
    crypto = state
    %{"price" => price, "amount" => amount, "type" => "trade"} = trade
    Common.update_price(@exchange, crypto, price, time, amount)
    {:ok, state}
  end

  ## Heartbeat
  def handle_msg(%{"type" => "heartbeat"}, state) do 
    {:ok, state}
  end  

  ## Other

  def handle_msg(msg, state) do 
    Common.log("#{@exchange}:#{state}", msg)
    {:ok, state}
  end

  defp url_string(crypto) do
    @url <> String.downcase(crypto) <> "usd?trades=true&heartbeat=true"  
  end

  #
  def cast_time(time) do
    Timex.from_unix(time, :millisecond)
  end
  
  def exchange_data do
    %{name: "Gemini",
      key: @exchange,
      cryptos: @cryptos}
  end

  def cryptos do
    @cryptos
  end
  # Common functions
  
  def handle_connect(conn, state) do
    Common.handle_connect(conn, state, @exchange)
  end

  def handle_disconnect(conn, state) do
    Common.handle_disconnect(conn, state, @exchange)
  end

  def child_spec(opts) do
    Common.child_spec(__MODULE__, opts)
  end
end
