defmodule CryptoArbitrage.DataSources.Gemini.Supervisor do
  alias CryptoArbitrage.DataSources.Gemini.Client
  alias CryptoArbitrage.DataSources.Common
  use Supervisor
  
  @name GeminiSupervisor

  @cryptos Client.cryptos
  @exchange :gemini

  def start_link(options \\ []) do
    Supervisor.start_link(__MODULE__, :ok, options ++ [name: @name])
  end

  def init(:ok) do
    Common.init_exchange(@exchange, @cryptos)
    children = Enum.map(@cryptos, 
      fn crypto -> Supervisor.child_spec({Client, crypto}, 
                                             id: "Gemini " <> crypto) end)
    options = [strategy: :one_for_one]
    Supervisor.init(children, options)
  end
end
