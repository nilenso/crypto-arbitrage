defmodule  CryptoArbitrage.DataSources.BitfinexClient do
  alias CryptoArbitrage.DataSources.Common
  use WebSockex

  @url "wss://api-pub.bitfinex.com/ws/2/"
  @cryptos ["BTC", "EOS", "ETH", "LTC", "XRP", "ZEC", "ETC"]
  @exchange :bitfinex

  def start_link(_opts) do
    {:ok, pid} = WebSockex.start_link(@url, __MODULE__, %{})
    Common.init_exchange(@exchange, @cryptos)
    subscribe_all(pid)
    {:ok, pid}
  end

  # Message handlers

  def handle_frame({:text, msg}, state) do
    handle_msg(Jason.decode!(msg), state)
  end

  ## Subscription 

  def handle_msg(%{"event" => "subscribed", 
                   "channel" => "trades",
                   "chanId" => chan_id,
                   "pair" => pair}, state) do
    crypto = Common.atomize_crypto(pair)
    new_state = Map.merge(state, %{chan_id => crypto})
    {:ok, new_state}
  end

  ## Trades
  def handle_msg([chan_id, "te", [_, time, amount, price]], state) do
    crypto = Map.get(state, chan_id)
    Common.update_price(@exchange, crypto, price, time, amount)
    {:ok, state}
  end

  ## "tu" Trades(ignore)
  def handle_msg([_, "tu", _], state) do
    {:ok, state}
  end

  ## Heartbeat
  def handle_msg([_, "hb"], state) do 
    {:ok, state}
  end

  ## Other

  def handle_msg(msg, state) do 
    Common.log(@exchange, msg)
    {:ok, state}
  end

  # Subscription helper functions

  defp subscribe_all(pid) do
    Enum.each(@cryptos, &subscribe(&1, pid))
  end

  defp subscribe(crypto, pid) do
    frame = subscription_frame(crypto)
    WebSockex.send_frame(pid, frame)
  end

  defp subscription_frame(crypto) do
    subscription_json =
      %{
        event: "subscribe",
        symbol: "t" <> crypto <> "USD",
        channel: "trades"
      }
      |> Jason.encode!()

    {:text, subscription_json}
  end
  
  #
  def cast_time(time) do
    Timex.from_unix(time, :millisecond)
  end

  def exchange_data do
    %{name: "Bitfinex",
      key: @exchange,
      cryptos: @cryptos}
  end
  # Common functions

  def handle_connect(conn, state) do
    Common.handle_connect(conn, state, @exchange)
  end

  def handle_disconnect(conn, state) do
    Common.handle_disconnect(conn, state, @exchange)
  end

  def child_spec(opts) do
    Common.child_spec(__MODULE__, opts)
  end

end
