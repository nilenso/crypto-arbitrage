defmodule CryptoArbitrage.DataSources.Common do
  alias CryptoArbitrage.DataSources
  alias CryptoArbitrage.DataServer 
  alias Timex
  require Logger

  @exchanges [DataSources.CoinbaseClient.exchange_data,
              DataSources.BitfinexClient.exchange_data,
              DataSources.BitstampClient.exchange_data,
              DataSources.KrakenClient.exchange_data,
              DataSources.Gemini.Client.exchange_data]

  @all_cryptos @exchanges |> Enum.map(fn exchange -> Map.get(exchange, :cryptos) end) |> Enum.concat |> Enum.uniq
 
  def atomize_crypto(str) do
    if is_atom(str) do
      str
    else
      formatted_str = str |> String.downcase |> String.replace("live_trades_", "") 
      |> String.replace("-", "") |> String.replace("/", "") |> String.replace("usd", "")
      cond do
        String.contains?(formatted_str, ["btc", "xbt"]) -> :btc
        String.contains?(formatted_str, ["eos"])        -> :eos
        String.contains?(formatted_str, ["eth"])        -> :eth
        String.contains?(formatted_str, ["ltc"])        -> :ltc
        String.contains?(formatted_str, ["bch"])        -> :bch
        String.contains?(formatted_str, ["xrp"])        -> :xrp
        String.contains?(formatted_str, ["bnb"])        -> :bnb
        String.contains?(formatted_str, ["zec"])        -> :zec
        String.contains?(formatted_str, ["etc"])        -> :etc
      end
    end
  end

  def init_exchange(exchange, cryptos) do
    init_cryptos = 
      Enum.map(cryptos, fn crypto -> {atomize_crypto(crypto), %{price: "NA", time: "NA", amount: "NA"}} end)
      |> Map.new
    DataServer.init_exchange(%{exchange => init_cryptos})
  end

  def update_price(exchange, crypto, price, time, amount)  do
    crypto = atomize_crypto(crypto)
    price = cast_to_float(price)
    time = cast_time(exchange, time)
    amount = cast_to_float(amount) |> abs
    DataServer.update_price(%DataServer.PriceUpdate{exchange: exchange, crypto: crypto,
                                                    price: price, time: time, amount: amount})
  end

  def all_cryptos do
    @all_cryptos
  end
 
  def exchanges do
    @exchanges
  end

  def log(exchange, msg) do
    title = "unhandled message" 
    log(title, exchange, msg)
  end
  
  def log(title, exchange, msg) do
    Logger.info("#{title}: #{exchange}\n#{inspect(msg)}\n")
  end

  def handle_connect(_conn, state, exchange) do
    log("Connected", exchange, "-")
    {:ok, state}
  end

  def handle_disconnect(_conn, state, exchange) do
    log("Disconnected", exchange, "-")
    {:ok, state}
  end

  def child_spec(module, opts) do
    %{
      id: module,
      start: {module, :start_link, [opts]},
      type: :worker,
      restart: :permanent,
      shutdown: 10000
    }
  end

  defp cast_to_float(price) do
    cond do
      is_binary(price)  -> String.to_float(price)
      is_integer(price) -> price / 1
      is_float(price)   -> price
    end         
  end
  
  defp cast_time(exchange, time) do
   date_time = 
      case exchange do
        :coinbase -> 
          DataSources.CoinbaseClient.cast_time(time)
        :bitfinex -> 
          DataSources.BitfinexClient.cast_time(time)
        :bitstamp -> 
          DataSources.BitstampClient.cast_time(time)
        :kraken -> 
          DataSources.KrakenClient.cast_time(time)
        :gemini ->
          DataSources.Gemini.Client.cast_time(time)
      end
   date_time
  end
end
