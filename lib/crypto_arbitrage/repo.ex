defmodule CryptoArbitrage.Repo do
  use Ecto.Repo,
    otp_app: :crypto_arbitrage,
    adapter: Ecto.Adapters.Postgres
end
