defmodule CryptoArbitrage.DataServer do
  use GenServer
  alias CryptoArbitrage.LiveUpdates

  @name DS
  defmodule PriceUpdate do
    @enforce_keys [:exchange, :crypto, :price, :time, :amount]
    defstruct [:exchange, :crypto, :price, :time, :amount]
  end

  #
  # Client API
  #

  def start_link(options \\ []) do
    GenServer.start_link(__MODULE__, :ok, options ++ [name: DS])
  end

  def init_exchange(init_data) do
    GenServer.cast(@name, {:init_exchange, init_data})
  end

  def update_price(%PriceUpdate{exchange: exchange, crypto: crypto,
                                price: price, time: time, amount: amount}) do
    GenServer.call(@name, {:update_price, exchange, crypto, price, time, amount})
  end

  def current_prices do
    GenServer.call(@name, :current_prices)
  end

  #
  # Callbacks
  #
  
  def init(:ok) do
    {:ok, %{}}
  end

  def handle_cast({:init_exchange, init_data}, state) do
    new_state = Map.merge(state, init_data)
    {:noreply, new_state}
  end

  def handle_call({:update_price, exchange, crypto, price, time, amount}, _from, state) do
    new_state = update_price(state, exchange, crypto, price, time, amount)
    LiveUpdates.notify_live_view({__MODULE__, [:price_update], []})
    {:reply, new_state, new_state}
  end
  
  def handle_call(:current_prices, _from, state) do
    {:reply, state, state}
  end

  #
  # Helper Functions
  #

  defp update_price(state, exchange, crypto, price, time, amount) do
    put_in(state, [exchange, crypto], %{price: price, time: time, amount: amount})
  end
end


