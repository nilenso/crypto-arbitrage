defmodule CryptoArbitrageWeb.PageController do
  use CryptoArbitrageWeb, :controller

  alias Phoenix.LiveView
  alias CryptoArbitrageWeb.PageLive.Index

  def index(conn, _params) do
    LiveView.Controller.live_render(conn, CryptoArbitrageWeb.PageLive.Index, session: %{})
  end
end



