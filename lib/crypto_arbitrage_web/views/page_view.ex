defmodule CryptoArbitrageWeb.PageView do
  alias CryptoArbitrage.DataSources.Common
  alias Timex
  use CryptoArbitrageWeb, :view

  defp format_price_or_amount(p_or_a) do
    cond do
      is_float(p_or_a) ->
        Float.round(p_or_a, 4)
      :default ->
        p_or_a
    end
  end

  defp time_elapsed_str(time) do
    cond do
      Timex.is_valid?(time) ->
        now = Timex.now
        {:ok, elapsed_str} = Timex.diff(now, time, :seconds) |> (fn seconds -> Timex.shift(now, seconds: (seconds * -1)) end).() |> Timex.format("{relative}", :relative)  
        elapsed_str
      :default ->
        time
    end
  end

  def arbitrage_delta_percent(current_prices, crypto) do
    crypto = Common.atomize_crypto(crypto)
    prices = current_prices
             |> Map.values
             |> Enum.map(&get_in(&1, [crypto, :price]))
             |> Enum.filter(&is_float(&1))
    {min, max} = Enum.min_max(prices, fn -> {1, 1} end)
    ((max - min)/min) * 100
  end
  
  def last_price_data(current_prices, exchange, crypto) do
    cond do
      crypto in exchange.cryptos ->
        crypto = Common.atomize_crypto(crypto)
        price = get_in(current_prices, [exchange.key, crypto, :price]) |> format_price_or_amount
        amount = get_in(current_prices, [exchange.key, crypto, :amount]) |> format_price_or_amount
        time = get_in(current_prices, [exchange.key, crypto, :time]) |> time_elapsed_str
        %{price: price, amount: amount, time: time}
      :default ->
        %{price: "", amount: "NA", time: "NA"}
    end
  end
end
