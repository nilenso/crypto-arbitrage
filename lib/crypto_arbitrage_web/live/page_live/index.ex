defmodule CryptoArbitrageWeb.PageLive.Index do
  use Phoenix.LiveView
  
  alias CryptoArbitrage.DataServer
  alias CryptoArbitrageWeb.PageView
  alias CryptoArbitrage.LiveUpdates
  alias CryptoArbitrage.DataSources.Common

  def mount(_session, socket) do
    crypto_data = Map.put_new(crypto_data, :current_prices, DataServer.current_prices())
    LiveUpdates.subscribe_live_view()
    {:ok, assign(socket, crypto_data: crypto_data)}
  end

  def handle_info({_from, _message, []}, socket) do
    crypto_data = Map.put_new(crypto_data, :current_prices, DataServer.current_prices())
    {:noreply, assign(socket, crypto_data: crypto_data)}
  end

  def render(assigns) do
    PageView.render("index.html", assigns)
  end

  defp crypto_data do
    %{:cryptos => Common.all_cryptos(), 
      :exchanges => Common.exchanges()} 
  end
end
