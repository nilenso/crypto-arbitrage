# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

config :crypto_arbitrage,
  ecto_repos: [CryptoArbitrage.Repo]

# Configures the endpoint
config :crypto_arbitrage, CryptoArbitrageWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "sRmbrOOup2JFVyOhSeH4kRogUmPrhgyXOxsvpLs0G3D6p+UJ3OVSDxkhuJP3Tc0K",
  render_errors: [view: CryptoArbitrageWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: CryptoArbitrage.PubSub, adapter: Phoenix.PubSub.PG2],
  live_view: [signing_salt: "Ak/QKT7oVfjqYHwfzynLEEQrTOd1825V"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Configure Tesla to use Hackney
config :tesla, adapter: Tesla, adapter: Tesla.Adapter.Hackney

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
